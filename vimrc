set nocompatible              " be iMproved, required
filetype plugin on
so ~/.vim/plugins.vim
set laststatus=2			"Set so vim-airline will show on all buffers
set tabstop=2				"Match rails generation tab size
set shiftwidth=2


syntax enable

set backspace=indent,eol,start
let mapleader = ','   			"Change default leader to comma
set number				"Activate line numbering"
set guifont=Menlo:h11
set linespace=3
"------------Visual-----------
set fillchars=""
"set background=light
let g:gruvbox_contrast_dark='hard'
set guioptions-=e			"We don't want GUI tabs
"Hide scrollbars
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R
set updatetime=250			"Updated visuals every 250ms

"-----------Split Management-----------
set splitbelow
set splitright
nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-L> <C-W><C-L>
nmap <C-H> <C-W><C-H>

"------------Search-----------
set hlsearch
set incsearch

"------------Mappings-----------
"Make it easy to edit the Vimrc File
nmap <Leader>ev :tabedit $MYVIMRC<cr>

"Add simple highlight removal
nmap <Leader><space> :nohlsearch<cr>

"Make NERDTree easier to toggle.
nmap <leader>ne :NERDTreeToggle<cr>

nmap <Leader>f :tag<space>
"------------------Plugins----------------"

"/
"/ CtrlP
"/
let g:ctrlp_custom_ignore = 'node_modules\DS_Store\|git'
let g:ctrlp_match_window = 'top,order:ttb,min:1,max:30,results:30'

nmap <D-p> :CtrlP<cr>
nmap <D-r> :CtrlPBufTag<cr>
nmap <D-e> :CtrlPMRUFiles<cr>

"/
"/ NERDTree
"/
let NERDTreeHijackNetrw=0

"/
"/ Airline-Theme
"
let g:airline_theme='gruvbox'

"/
"/ signify
"/
let g:signify_vcs_list = [ 'git', 'hg' ]

"/
"/ silver_searcher
"/
let g:ackprg = 'ag --nogroup --nocolor --column'

"------------Auto-Commands------------"
"Automatically source the Vimrc file on save"
augroup autosourcing
	autocmd!
	autocmd BufWritePost .vimrc source %
augroup END

"Auto start NERDTree on start"
let g:NERDTreeHijackNetrw = 1
"au VimEnter NERD_tree_1 enew | execute 'NERDTree '.argv()[0]

hi VertSplit ctermbg=NONE guibg=NONE
