set nocompatible              " be iMproved, required
filetype off                  " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-vinegar'
Plugin 'scrooloose/nerdtree'
Plugin 'kien/ctrlp.vim'
Plugin 'bling/vim-airline'
"Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-rails'
Plugin 'scrooloose/nerdcommenter'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'nfvs/vim-perforce'
Plugin 'mhinz/vim-signify'
Plugin 'morhetz/gruvbox'
Plugin 'takac/vim-spotifysearch'
Plugin 'kristijanhusak/vim-hybrid-material'
Plugin 'dracula/vim'
call vundle#end()            " required

filetype plugin indent on    " required
